#!/usr/bin/python3
#
# MIT License
#
# Copyright (c) 2017 Jesus Vedasto Olazo

import tkinter as tk
import tkinter.messagebox as mb
import tkinter.filedialog as fd
import sys
import os
import hashlib

__version__ = '1.0.0'

class Application(tk.Frame):

    def __init__(self, master=None, **kwargs):
        tk.Frame.__init__(self, master, kwargs)
        self.master.protocol('WM_DELETE_WINDOW', self.quitApp)
        self.master.title('TkCheckSum - '+__version__)
        if os.name == 'nt':
            try:
                set_icon = os.getcwd()+'\\locked.ico'
                self.master.iconbitmap(set_icon)
            except:
                error = sys.exc_info()[1]
                mb.showerror('Icon Error',
                             'Application icon not found!\n\n'+str(error)
                             )
        elif os.name == 'posix':
            try:
                icon = tk.PhotoImage(file=os.getcwd()+'/locked.gif')
                self.master.tk.call('wm', 'iconphoto', self.master._w, icon)
            except:
                print(sys.exc_info()[1])
        self.pack(expand=True, fill='both')
        self.setupUI()

    def setupUI(self):
        # Choose checksum type.
        self.checksum_frame = tk.LabelFrame(self, text="CheckSum Type",
                                            labelanchor='nw')
        self.checksum_frame.pack(padx=10, pady=10)
        MODES = [('MD5', 'md5'),
                 ('SHA1', 'sha1'),
                 ('SHA224', 'sha224'),
                 ('SHA256', 'sha256'),
                 ('SHA384', 'sha384'),
                 ('SHA512', 'sha512')
                 ]
        self.chksum_var = tk.StringVar()
        self.chksum_var.set(MODES[0][1])

        for text, mode in MODES:
            self.radio_btn = tk.Radiobutton(self.checksum_frame, text=text,
                                            variable=self.chksum_var,
                                            value=mode)
            self.radio_btn.pack(side='left')
            self.radio_btn.bind('<ButtonRelease>', self.changeType)

        # Choose file section.
        self.file_frame = tk.LabelFrame(self, text="Filename",
                                        labelanchor='nw')
        self.file_frame.pack(fill='x', padx=10, pady=10)
        self.file_entry = tk.Entry(self.file_frame, bg='#f0f8ff')
        self.file_entry.pack(expand=True, fill='x', side='left',
                             padx=5, pady=5)
        self.browse_btn = tk.Button(self.file_frame, text='Browse',
                                    width=15, bg='blue', fg='white',
                                    command=self.chooseFile)
        self.browse_btn.pack(padx=5, pady=5)

        self.gen_checksum_btn = tk.Button(self, text="Generate MD5 Checksum",
                                          width=25, bg='blue', fg='white',
                                          command=self.genChkSum)
        self.gen_checksum_btn.pack(padx=5, pady=5)

        # Compare generated checksum from to an external source.
        self.compare_frame = tk.LabelFrame(self, text="Compare MD5 Checksums",
                                           labelanchor='nw')
        self.compare_frame.pack(fill='x', padx=10, pady=10)
        self.gen_chksum_entry = tk.Entry(self.compare_frame, bg='#f0f8ff')
        self.gen_chksum_entry.pack(expand=True, fill='both',
                                   padx=5, pady=5)
        self.in_chksum_entry = tk.Entry(self.compare_frame, bg='#f0f8ff')
        self.in_chksum_entry.pack(expand=True, fill='both', padx=5, pady=5)
        self.result_label = tk.Label(self.compare_frame, text='Result',
                                     bg='yellow', fg='blue')
        self.result_label.pack(expand=True, fill='both', padx=5, pady=5)
        self.compare_btn = tk.Button(self.compare_frame, text='Compare',
                                     command=self.compareChkSum, width=12,
                                     bg='blue', fg='white')
        self.compare_btn.pack(padx=5, pady=5, side='left')
        self.save_btn = tk.Button(self.compare_frame, text='Save',
                                  command=self.saveCheckSum, width=12,
                                  bg='blue', fg='white'
                                  )
        self.save_btn.pack(side='left')

        # Close the application.
        self.close_btn = tk.Button(self, text="Close",
                                   command=self.quitApp,
                                   width=25, bg='blue', fg='white')
        self.close_btn.pack(padx=5, pady=5)

        # Shows copyright.
        copy_right = 'Copyright (c) 2017 Jesus Vedasto Olazo'
        self.copy_lbl = tk.Label(self, text=copy_right)
        self.copy_lbl.pack(padx=5, pady=5)

        self.file_entry.focus_set()

    def saveCheckSum(self):
        if self.file_entry.get() == '':
            mb.showerror('Invalid Path', 'Invalid path.')
            return None
        filepath = os.path.splitext(self.file_entry.get())[0]
        filename = filepath + '.' + self.chksum_var.get()
        checksum = self.gen_chksum_entry.get()
        if checksum == '':
            mb.showerror('Checksum Error', 'Please generate checksum.')
            return None
        with open(filename, 'w') as txtfile:
            txtfile.write(checksum)
        if os.path.isfile(filename):
            mb.showinfo('Success', 'Checksum has been saved.')
        else:
            mb.showerror('Error', 'Something went wrong.')

    def changeType(self, event):
        value = event.widget.cget('text')
        self.gen_checksum_btn.config(
            text=" ".join(['Generate', value, 'Checksum'])
            )
        self.compare_frame.config(
            text=" ".join(['Compare', value, 'Checksums'])
            )

    def compareChkSum(self):
        chksum_gen = self.gen_chksum_entry.get()
        if chksum_gen == '':
            mb.showerror('Invalid Checksum!', 'Checksum cannot be empty.')
            return None
        chksum_in = self.in_chksum_entry.get()
        check = False
        chksum_in_test = [value for value in chksum_in]
        for elem in chksum_in_test:
            if elem.islower():
                check = True
        if check:
            self.in_chksum_entry.delete(0, tk.END)
            chksum_in = chksum_in.upper()
            self.in_chksum_entry.insert(0, chksum_in)
        if chksum_in == '':
            mb.showerror('Invalid Checksum', 'Checksum cannot be empty.')
            return None
        if chksum_gen == chksum_in:
            self.result_label.config(bg='green', fg='yellow', text='Match')
        else:
            self.result_label.config(bg='red', fg='yellow', text='Not Match')

    def genChkSum(self):
        filename = self.file_entry.get()
        if filename == '':
            mb.showerror('No File.', 'No file has been selected.')
            return None
        chksum = self.chksum_var.get()
        try:
            result = self.checkSums(chksum, filename)
        except FileNotFoundError:
            error = sys.exc_info()[1]
            mb.showerror('File Not Found', 'File not found!\n\n'+str(error))
            return None
        if self.gen_chksum_entry.get() != '':
            self.gen_chksum_entry.delete(0, tk.END)
        self.gen_chksum_entry.insert(0, result)

    def checkSums(self, chksum_type, in_file, block_size=128):
        if chksum_type == 'md5':
            chksum = hashlib.md5()
        elif chksum_type == 'sha1':
            chksum = hashlib.sha1()
        elif chksum_type == 'sha224':
            chksum = hashlib.sha224()
        elif chksum_type == 'sha256':
            chksum = hashlib.sha256()
        elif chksum_type == 'sha384':
            chksum = hashlib.sha384()
        else:
            chksum = hashlib.sha512()

        size_of_file = os.stat(in_file).st_size
        with open(in_file, 'rb') as input_file:
            while True:
                data = input_file.read(block_size)
                if not data:
                    break
                chksum.update(data)
        return chksum.hexdigest().upper()

    def chooseFile(self):
        cur_dir = os.getcwd()
        filename = fd.askopenfilename(initialdir=cur_dir, title='Open File',
                                      parent=self)
        if self.file_entry.get() != '':
            self.file_entry.delete(0 , tk.END)
        self.file_entry.insert(tk.END , filename)

    def quitApp(self):
        self.master.destroy()


if __name__ == '__main__':
    app = Application()
    app.mainloop()
